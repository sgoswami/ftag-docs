# Secondary vertex based low-level algorithms

The identification particles originating from a place different to where the b-quark is used to infer the presence of a b-jet.
Two classes of algorithms are used to reconstruct these secondary vertices, whose properties are used in algorithms.

More documentation (in TWiki) for low-level taggers:

- [SV](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BtaggingSV)
- [JetFitter](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingJetFitter)

## SV

The [SV](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BtaggingSV) family of vertex based low-level algorithms makes use of two types of software. First, vertex finding algorithms are run to identify the secondary vertices in an event. Then, the vertex based jet tagging algorithms compute discriminants based on the properties of secondary vertices, which can be used to identify b-jets.

### Secondary Vertex Finders

There are two types of vertex finding algorithms: SSVF (single secondary vertex finder) and MSVF (multi secondary vertex finder).
Both are based on the [VKalVrt](https://cds.cern.ch/record/685551) vertex reconstruction package.
Reconstructed secondary vertices provide information about the vertex mass, energy fraction in the secondary vertex w.r.t. the jet, and the number of two-track vertices associated with it.

### Code in Athena:

- [VKalVrt](https://gitlab.cern.ch/atlas/athena/-/tree/21.2/Reconstruction/VKalVrt) (R21)
- [InDetVKalVxInJetTool](https://gitlab.cern.ch/atlas/athena/-/tree/21.2/InnerDetector/InDetRecTools/InDetVKalVxInJetTool/) (R21)


#### SSVF

The single secondary vertex finder (SSVF) is an algorithm which identifies a single secondary vertex per jet.
Tracks associated with the jet are selected if they pass certain cuts. From these tracks, a list of two-track vertices is formed. The list is cleaned by mass requirements to reject those originating from K0 and Λ decays. The tracks of all surviving two-track vertices are used as inputs for an iterative vertex finder, trying to fit one SV out of all these tracks. In case no secondary vertex is reconstructed, a pseudo-vertex is formed from the crossing of the jet axis and a high-impact-parameter track to enhance the efficiency in high pt regimes. 

#### MSVF
The multi secondary vertex finder (MSVF) is an algorithm which can identify multiple secondary vertices per jet.
It shares the first steps until arriving at a set of two-track vertices with the SSVF. This set is translated into a graph indicating the compatibility of tracks for vertices with unambiguous track assignment. Using an iterative cleaning procedure, vertices are re-fitted until they provide disjoint partitions of tracks.

### Secondary Vertex (SV)-based taggers

The SVx algorithms are based on secondary vertex information. These algorithms rely on the output of the secondary vertex finders and construct likelihood ratios out of the ratio of b-jet probability density function to the light-jet probability density function, which are used as discriminative variables.

The SVx taggers employ five input variables from the secondary vertices:

- mass of reconstructed secondary vertex,
- ratio of total energy of tracks in the SV to the total energy of tracks in the jet,
- number of good two-track vertices,
- $\Delta R$ distance between the jet direction and the secondary vertex-primary vertex line,
- significance for 3D distance between secondary and primary vertex.

#### SV1

The SV1 tagger is based on the inputs provided by the SSVF.
The following table lists the variables which it can provide to the high level taggers.

| variable             | description |
| -------------------- | ----------- |
| `SV1_masssvx`        | invariant mass of tracks at the secondary vertex assuming pion mass |
| `SV1_efracsvx`       | energy fraction of the tracks associated with the secondary vertex w.r.t. jet energy |
| `SV1_significance3d` | distance between the primary and the secondary vertex divided by its uncertainty |
| `SV1_dstToMatLay`    | distance from secondary vertex to the closest material layer |
| `SV1_deltaR`         | $\Delta R$ distance between the jet axis and the direction of the secondary vertex relative to the primary vertex |
| `SV1_Lxy`            | transverse distance between the primary and secondary vertex |
| `SV1_L3d`            | distance between the primary and the secondary vertex |
| `SV1_N2Tpair`        | number of two-track vertex candidates |
| `SV1_NGTinSvx`       | number of tracks used in the secondary vertex |


## JetFitter

The [JetFitter](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingJetFitter) algorithm exploits the topological structure of weak b- and c-hadron decays inside the jet. A Kalman filter is used to find a common line on which the primary vertex and the b- and c-quark vertices lie, as well as their position on this line approximating the b-hadron flight path.
The following table lists the variables which it can provide to the high level taggers.

| variable                   | description |
| -------------------------- | ----------- |
| `JetFitter_nVTX`           | number of vertices with more than one track |
| `JetFitter_nSingleTracks`  | number of single track vertices |
| `JetFitter_nTracksAtVtx`   | number of tracks from multi-prong displaced vertices |
| `JetFitter_N2Tpair`        | number of two-track vertex candidates (prior to decay chain fit) |
| `JetFitter_energyFraction` | fraction of the charged jet energy in the secondary vertices |
| `JetFitter_mass`           | invariant mass of the tracks fitted to the vertices with at least two tracks |
| `JetFitter_significance3d` | significance of the average distance between PV and displaced vertices, considering all multi-prong vertices or (if there are none) of all single-track vertices |
| `JetFitter_deltaphi`       | azimuthal distance $\Delta \phi$ between sum of all momenta at vertices and the fitted B-meson flight direction |
| `JetFitter_deltaeta`       | pseudorapidity distance $\Delta \eta$ between sum of all momenta at vertices and the fitted B-meson flight direction |
| `JetFitter_massUncorr`     |             |
| `JetFitter_dRFlightDir`    |             |
| `JetFitter_deltaR`         |             |


The algorithm and its performance are documented in greater detail in [ATL-PHYS-PUB-2018-025](https://cds.cern.ch/record/2645405/files/ATL-PHYS-PUB-2018-025.pdf).

### Code in Athena (R22):

- `InDetSecVxFinderTool/InDetImprovedJetFitterVxFinder.h`
	- `athena/InnerDetector/InDetRecTools/InDetSecVxFinderTool/`: [header](https://gitlab.cern.ch/atlas/athena/-/blob/main/InnerDetector/InDetRecTools/InDetSecVxFinderTool/InDetSecVxFinderTool/InDetImprovedJetFitterVxFinder.h) | [source](https://gitlab.cern.ch/atlas/athena/-/blob/main/InnerDetector/InDetRecTools/InDetSecVxFinderTool/src/InDetImprovedJetFitterVxFinder.cxx)
- `VxSecVertex/VxSecVertexInfo.h`
	- `athena/Tracking/TrkEvent/VxSecVertex/`:  [header](https://gitlab.cern.ch/atlas/athena/-/blob/main/Tracking/TrkEvent/VxSecVertex/VxSecVertex/VxSecVertexInfo.h) | [source](https://gitlab.cern.ch/atlas/athena/-/blob/main/Tracking/TrkEvent/VxSecVertex/src/VxSecVertexInfo.cxx)
- `VxSecVertex/VxJetFitterVertexInfo.h`
	- `athena/Tracking/TrkEvent/VxSecVertex/`: [header](https://gitlab.cern.ch/atlas/athena/-/blob/main/Tracking/TrkEvent/VxSecVertex/VxSecVertex/VxJetFitterVertexInfo.h) | [source](https://gitlab.cern.ch/atlas/athena/-/blob/main/Tracking/TrkEvent/VxSecVertex/src/VxJetFitterVertexInfo.cxx)