# Systematic strategies

Each heavy-flavour tagger calibration analysis provides not only the efficiency scale-factor *central values*, but also the corresponding **systematic variation histograms**. These are all stored together in the **CDI files** which are centrally available to any ATLAS analysis. 

One decision an analyser must make (in addition to which calibration to use in their analysis) is in **how they want to handle these systematic uncertainties**. There are a few so-called **systematic strategies** that are currently available - the recommended strategy being an eigenvector decomposition method which reduces the total number of variations needed at the analysis level via a principal component analysis (PCA). More information about this strategy can be found later in this document.


The currently available selection of systematic strategies available is as follows:

| Strategy | Description |
| :-------------------- | ---------------- |
| **Envelope**        | Only the total uncertainties will be reported for each jet flavour, as well as two extrapolation uncertainties, `FT_EFF_extrapolation` and `FT_EFF_extrapolation_from_charm`.  This is an acceptable method to asses whether b-tagging uncertainties affect your analysis, but should not be used for publication if they do. |
| **SFEigen**      | Perform an eigenvector decomposition, extracting only the largest contributing systematic variations in the form of so-called *eigenvector variations*. This approach provides accurate knowledge of bin-to-bin correlations for each jet flavour individually, and as such, the usage of this systematic strategy is generally recommended.    |
| **SFGlobalEigen**   | Similar to the **SFEigen** strategy, this strategy produces eigenvector variations on jets, but it performs the PCA on *all jet flavours simultaneously*, treating uncertainties that are correlated between jet flavours in a statistically more correct manner. Currently a work-in-progress as reported in [AFT-578](https://its.cern.ch/jira/browse/AFT-578)         |
 


The [`xAODBTaggingEfficiency`](https://gitlab.cern.ch/atlas/athena/-/tree/master/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency) package provides a high-level interface that simplifies usage of flavour-tagging calibrations. In most use-cases, analysers should utilise this interface to set-up any of the above systematic strategies. Specifically, one should set up the [`xAODBTaggingEFficiencyTool`](https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingEfficiencyTool.h) similar to how it is shown in [this example code](https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/util/BTaggingEfficiencyToolTester.cxx). More information about this interface can be found later in this document.
