# Large Radius Tracking (LRT)
# Training DIPS with LRT

This documentation keeps track of the DIPS training efforts with LRT. The Gitlab CI will be implemented soon for the LRT workflow. The following steps are computationally involved and resource intensive overall. Advised to start your workflow on a cluster with ample resources. 

A large part of important plots and important resources are gathered here: [AQP AFT-543 Docs](https://atlasaqpaft543.web.cern.ch/index.php)

# Training Sample Production (TDD)

In a fresh shell, start by setting up a new work area. Please note that the current TDD branch is named `allvar` since it can run over LRT based DAODs with all the variables in the jet and track config files as one would do for other samples. It will be renamed as `lrt` soon.
```
setupATLAS --quiet
lsetup git
git clone -b lrt https://gitlab.cern.ch/sgoswami/training-dataset-dumper.git 
source training-dataset-dumper/setup-athena.sh
mkdir build run
cd build
cmake ../training-dataset-dumper
make -j$(nproc)
cd ../
source build/x*/setup.sh
```

This should have built the package succesfully with Athena. Please check the Athena version.

To set up grid environment, try these steps:
```
source training-dataset-dumper/BTagTrainingPreprocessing/grid/setup.sh
source build/x*/setup.sh
```
Then jobs can be submitted from the working directory by editing the `grid-submit` script under `BTagTrainingPreprocessing/grid/grid-submit` and then running `grid-submit -f lrt`

## FTAG1 Derivations 

The recent FTAG1 derivations can be found in this link: 
[Prod link](https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/41610/)

Samples with r13145 (or p4974 and up) tag has the LRT track container saved in the output.

The AOD for long-lived-particles (LLP) sample is: `mc20_13TeV:mc20_13TeV.600918.PhPy8_VBF_H125_a55a55_4b_ctau10.recon.AOD.e8342_s3681_r13145`

The corresponding FTAG1 DAOD dataset is: `mc20_13TeV.600918.PhPy8_VBF_H125_a55a55_4b_ctau10.deriv.DAOD_FTAG1.e8342_e7400_s3681_r13145_p4974`

The ttbar samples with LRT+STD tracks can already be used for comparison. The corresponding FTAG1 derivation is:
`mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3681_r13144_r13146_p4974`

## Produce the h5 Files

To produce the h5 files, you should run the following if you want to merge the LRT and standard tracks:

```
cd run
ca-dump-lrt -c ../training-dataset-dumper/configs/single-b-tag/minimal_LRT.json input_DAOD_files_to_run -M
```
If you DO NOT want to merge the LRT and standard tracks, run (We used above, the `-M` argument for Merging Track Containers):
```
cd run
ca-dump-lrt -c ../training-dataset-dumper/configs/single-b-tag/minimal_LRT.json input_DAOD_files_to_run 
```
You can currently and should use: `EMPFlow.json` with the `-c` option.

The config file imports so-called fragments with universally used properties for the configuration, including the cuts on track impact parameters, so choose the one that suits you. See Umami cuts section below. For standard tracks, use `ip3d-track-cuts.json` and for looser cuts, use `ip3d-loose-track-cuts.json` .

# Umami Training Framework
Get the package by doing:
```
git clone -b lrtndips https://:@gitlab.cern.ch:8443/sgoswami/umami.git
```
The Umami docs can be found here: [Umami Docs](https://umami-docs.web.cern.ch/) whereas the LRT based DIPS codebase is here: [Umami LRT+DIPS](https://gitlab.cern.ch/sgoswami/umami/-/blob/lrtndips/)

### Cuts (Experimental: TBD)
It is very important to apply the specific d0 and z0 cuts. If your h5 files were not already produced with the cuts, you may apply them as cuts in the yaml preprocessing file for DIPS in the same format as the outlier cuts are applied. Don't forget to enforce cuts in the training blocks when doing event selections to be used for training, testing and validation.

The typical values for std(regular) (d0,z0) cuts are (1,1.5) and the DIPS loose cuts are (3.5,5.0). These are also specified in the frag files in TDD processing. There could be other values, so make note of what configuration you're using.

Umami doesn't yet have, as of 11 July, 2022, a way to introduce track cuts directly in the pre-processing stage. If there is one in the future release, this would need to be implemented. 

### Introducing Track IP Cuts
Since there's no direct way in Umami, we have another improvisation that runs locally,however,it is in no way time efficient.

Please have a look here:
[Track Filter](https://github.com/SAMMY0909/track_filter)

A [post processing script](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/GNNJetTagger/-/blob/main/gnn_tagger/postprocessing/postprocessing.py) in GNN repo may be adapted to your needs to implement the track cuts.

To run the scripts, make sure you have basic Python3 packages such as Numpy, Pandas, OS etc installed for a given Python 3.x version. 

### Preprocessing (e.g DIPS)

This step prepares the training inputs using the h5 files produced before. It will sample the jets according to a specific setup. A prototype pre-processing workflow is given here (Same for the ones with LRT/ other triggers):

```
python3 umami/preprocessing.py -c examples/DIPS/PFlow-DIPS-Preprocessing.yaml  --sample training_ttbar_bjets  --prepare
python3 umami/preprocessing.py -c examples/DIPS/PFlow-DIPS-Preprocessing.yaml  --sample training_ttbar_cjets  --prepare
python3 umami/preprocessing.py -c examples/DIPS/PFlow-DIPS-Preprocessing.yaml  --sample training_ttbar_ujets  --prepare

python3 umami/preprocessing.py -c examples/DIPS/PFlow-DIPS-Preprocessing.yaml  --sample testing_ttbar  --prepare

python3 umami/preprocessing.py -c examples/DIPS/PFlow-DIPS-Preprocessing.yaml  --sample validation_ttbar  --prepare

python3 umami/preprocessing.py -c examples/DIPS/PFlow-DIPS-Preprocessing.yaml  --resampling 
python3 umami/preprocessing.py -c examples/DIPS/PFlow-DIPS-Preprocessing.yaml  --scaling
python3 umami/preprocessing.py -c examples/DIPS/PFlow-DIPS-Preprocessing.yaml  --apply_scales
python3 umami/preprocessing.py -c examples/DIPS/PFlow-DIPS-Preprocessing.yaml  --write
python3 umami/preprocessing.py -c examples/DIPS/PFlow-DIPS-Preprocessing.yaml  --to_records #Only if you need TF type files

```

### Training (e.g. DIPS)
Again, the prototype steps are:

```
python3 umami/train.py -c examples/DIPS/DIPS-PFlow-Training-config.yaml -e 200 

python3 umami/plotting_epoch_performance.py -c examples/DIPS/DIPS-PFlow-Training-config.yaml --dict umami/modelDIPSstd/validation_WP0p77_600000jets_Dict.json
```
To recalculate, not needed in first training
```
python3 umami/plotting_epoch_performance.py -c examples/DIPS/DIPS-PFlow-Training-config.yaml  --recalculate 
```
Choose the best epoch from the rejection vs epoch curve and accuracy vs epoch curve. Avoid unstable regions
```
python3 umami/evaluate_model.py -c examples/DIPS/DIPS-PFlow-Training-config.yaml -e 59
```
Make the plots
```
python3 umami/plotting_umami.py -c examples/DIPS/plotting_umami_config_DIPS.yaml -o dips_eval_plots_std -f png

```
# Running Umami: Detailed Steps
### Lxplus python prerequisites
Requires `>=python3.8`, `<=python3.9`.
Please note that running any Neural Network training on lxplus is not the best idea. In all likelihood, you will not get good nodes with Intel Xeon scalable processors and the memory isn't ECC which significantly hampers intensive I/O ops. The systems may be slow and jobs might crash more often than expected. Anything slightly memory intensive is typically terminated instantly. 
```
setupATLAS -q
scl enable rh-python38 bash
python3 -m ensurepip --upgrade --user
python3 -m pip  install --user --upgrade pip
python3 -m pip install --upgrade pip --user
python3 -m pip install h5py --user
python3 -m pip  install scipy==1.8.0 --user
python3 -m pip  install --user -r requirements.txt
python3 -m pip  install --user -r requirements_additional.txt
python3 -m pip  install --user -r requirements_develop.txt
source run_setup.sh develop
```
Please note that Umami may be exceptionally sensitive to the python package versions. You may need to debug on your own.

### Plotting Jet & Track Input variables
Plots for single dataset as well as comparison plots between two datasets can be made. See the scripts under `examples/DIPS/varplotconfigs/`
```
plot_input_variables.py -v -c examples/DIPS/varplotconfigs/jetconf.yaml --jets -f png
plot_input_variables.py -v -c examples/DIPS/varplotconfigs/ntrackconf.yaml --tracks -f png
plot_input_variables.py -v -c examples/DIPS/varplotconfigs/trackconf.yaml --tracks -f png
```
### Initial Data Prep
First convert your ROOT files to h5 files using a framework/code of your choosing, like [TDD](https://gitlab.cern.ch/sgoswami/training-dataset-dumper/-/tree/allvar)
Make sure that the variables in the h5 files are properly written out from the respective branches of the ROOT files and are mapped one to one in your h5 files. Different taggers will have different requirements. If the required data is not present in your initial ROOT files, your code will crash in the absence of valid pythonic 'keys'. In the preprocessing, training and other steps, in the yaml config files, the directories need to be correctly specified.

### Prototype raw h5 dataset storage directory structure
Put standard track samples under std and merged track samples under lrt.
```
├── llp
│   ├── lrt
│   └── std
└── ttbar
    ├── lrt
    └── std
```

### Your prototype preprocessed data storage directory structure
Just define `DIPS`,`DIPS/std` & `DIPS/lrt`, the rest will be taken care of by Umami.
```
--- DIPS
    --- lrt
    -   --- hybrids
    -   --- preprocessed
    -       --- plots
    -           --- resampling
    -           -   --- tracks_loose
    -           --- resampling_scaled_shuffled
    -           -   --- tracks_loose
    -           --- scaling
    -               --- tracks_loose
    --- std
        --- hybrids
        --- preprocessed
            --- plots
                --- resampling
                -   --- tracks_loose
                --- resampling_scaled_shuffled
                -   --- tracks_loose
                --- scaling
                    --- tracks_loose
```

### Git Stuff
Make changes to the script `commitgit.sh` to commit your git changes

```
source commitgit.sh
```

### Running Umami
Note that it can be run in hybrid mode. The package has to be cloned normally first and then the docker image has to be fired up.

Put this in your `.bashrc` file:
```
export SINGULARITY_CACHEDIR=/home/sammy/afs/cern.ch/work/${USER:0:1}/${USER}/public/singularity
```

### Docker Image
These are the steps to run an Umami docker image:

```
git clone -b lrtndips https://:@gitlab.cern.ch:8443/sgoswami/umami.git
cd umami
```
Required first time if you haven't created the singularity cache folder:
```
mkdir /afs/cern.ch/work/${USER:0:1}/${USER}/public/singularity
```
Required first time if you haven't created the docker image (or simg) storage folder:
```
mkdir /afs/cern.ch/work/${USER:0:1}/${USER}/public/dockimage/
singularity pull /afs/cern.ch/work/${USER:0:1}/${USER}/public/dockimage/umami_base_cpu.img docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/algorithms/umami/umamibase:latest
```
To execute Singularity (if you need Nvidia GPU support, type --nv after exec)
```
singularity exec --bind /eos,/cvmfs,/afs --home $PWD /afs/cern.ch/work/${USER:0:1}/${USER}/public/dockimage/umami_base_cpu.img /bin/bash
```
### To start singularity using a script
Simply edit the script `singstartup.sh` to your needs with the appropriate directories present and do
```
source singstartup.sh
```
This will give an option whether to use CPU(1) or GPU(2) mode.
Then, from within singularity:
```
Singularity> source ~/.bashrc
Singularity> kinit
Singularity> source run_setup.sh 
```
The last step is important, it sets up scripts' linkings.

### Manual setup
Discouraged, but one could run steps similar to the lxplus ones by replicating the directory structure.

### Python Issues
Known issues are version clashes where you have some package not found errors. Try this fix: `unset PYTHONPATH` No guarantee that it'll work, but it does work in most cases.

### Refining h5 files with preprocessing jobs
Preprocessing has to be done from within the singularity shell. This prepares the training and testing files. First you have to adapt all the `.yaml` configuration files, under this directory: `umami/examples/` to your needs.

```
Singularity> source run_setup.sh
```
For DIPS tagger on STD dataset: Preprocessing
```
Singularity> source execute_pre_DIPS.sh
```
For DIPS tagger on LRT+STD (combined track) dataset: Preprocessing
```
Singularity> source execute_pre_LRT_DIPS.sh
```

### Training
The training steps, after sample creation, can be started from within or outside singularity (if your shell environment has all the dependencies installed and proper environment variables are sourced) right at the `umami` folder level.

In those steps, the python files, during runtime, try to access NVIDIA GPU devices. Make sure, for that step/those steps, your singularity is fired up in GPU mode instead of the default CPU mode.

Please note that epoch arguments whenever needed, can be passed by `-e <epoch number>`; the output folder and plot file type may be supplied respectively by `-o <foldername>` and `-f <png or pdf>`

Refer to the main documentation for details.

To run  the training steps:
For DIPS tagger on STD dataset
```
lxplus>source execute_DIPS.sh
```
For DIPS tagger on STD+LRT dataset
```
lxplus>source execute_LRT_DIPS.sh
```

## Plotting Scripts for ROCs and Other Vars
You may use these scripts as guideline to plot your ROCs and dependence of Efficiency/Rejection etc. on a given variable. The other scripts are also available in the Umami repo itself on lrtndips branch.

[ROC PLOT SCRIPS](https://github.com/SAMMY0909/UmamiRocPlots)

or, you can use examples from [PUMA-HEP](https://umami-hep.github.io/puma/main/index.html)

# Troubleshooting Common Errors

### LXPLUS
`DatasetDumper.output  ERROR ServiceLocatorHelper::service: can not locate service H5FileSvc/H5FileSvc`

This smees to be triggered by the output service. Make sure the previous job writing to the same output file is killed and clean things up. This usually solves the issue.

Sometimes the git setup doesn't push your changes to your branch. This is primarily because of stray environment variables sourced in your script. Resolution: Use another lxplus node or terminal.

While logging in to lxplus, it may say: `unable to getcwd()`, it means that the lxplus node shell isn't behaving properly. Exit and log in to another node.

### TDD
If you see crashes during runtime and things like `Missing SG Aux variable`, check your master json config file and your frag config file to see if it's because of the variables declared there. Remove variables as wanted.

Regular xAODs can't be used with `ca-dump-lrt` since there's no b-tagging object. You need FTAG1 derivations `p4974` or up.

### Umami Framework
In the training step, if it complains about cuts: Check the outlier/regular cuts and remove/change those fields.

If it complains about a variable being dict and not put in the right format, use curly braces to supply dict arguments `{item1,item2}` etc.

If the preprocessing is going fine and then it crashes because it's unable to write to a file, it means LXPLUS is unable to handle such buffers due to high node load, etc. Best if you can afford your own machine. It takes quite a lot of bouncing around to get resources.

### Grid Errors
[Grid Job Debugging](https://twiki.cern.ch/twiki/bin/view/PanDA/PandaAthena#6_How_to_debug_jobs_when_they_fa)
If you see a CE transform error, the `x86*/setup.sh` wasn't sourced.

If you see a Pilot Transform error 127/133/1305, check the `payload.stdout` file under logs, it might be a missing variable or an improperly referenced script or a combination of those.
