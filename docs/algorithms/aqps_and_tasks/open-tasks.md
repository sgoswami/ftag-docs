# Open Tasks/AQPs

The following tasks are open.
Some (larger) tasks may be suitable for authorship qualification projects.
In case you would like to start one of these projects in the flavour tagging algorithm group, [contact the subgroup conveners](mailto:atlas-cp-flavtag-algorithms-conveners@cern.ch).
Smaller tasks may also serve as inspiration.

These are not exhaustive lists.


### Larger Projects:


??? example "Systematic aware trainings"

    goals: train taggers with systematics applied to inputs to reduce data/mc discrepancies

    - it would be great to apply systematics on the fly during training as a form of data augmentation
    - this would require some work to convert the CP tools to python
    - alternative approach is to dump with systematics via the tdd and combine nom + sys jets during preprocessing
    - add variation to inputs may help with overtraining, see [AFT-699](https://its.cern.ch/jira/browse/AFT-699)


??? example "MC Sample studies"

    goals: improve tagger robustness to mismodelling

    - investigate performance in different MC generators
    - R&D: find procedure to reduce dependence on training samples
        - e.g. train on all MC sample variations
    - study difference between l+jets and dilepton ttbar
    - automise workflow of validating samples - diagnosis
        - come up with set of checks/plots


??? example "Improved NN Vertex Reconstruction"

    - move beyond pairwise comparisons of tracks to a more sophisticated approach, e.g. maskformer
        - multitrack vertex finding is done within the model, rather than as a post-processing step
    - properties of the vertices can also be regressed directly, removing the need for a vertex fitting stage
        - requires some work on the software side to make the truth targets available in the h5
        - altneratively, can implement differentiable vertex fitting layer
    - test the above in X-bb setting with the aim of reconstructing the full decay chains
    - more information in [vertexing activity page](https://ftag-docs.docs.cern.ch/activities/tracking/gnn-vertexing/)


??? example "Expanding Available Truth Info"

    goals: improve light calibration, tagging performance

    - implement more granular light jet labelling with separate labels for light, long lived light, and light with material interaction
    - full truth vertex info, including truth vertex type, properties, and outgoing tracks
    - investigate imapct of geant slimming: https://its.cern.ch/jira/browse/ATLMCPROD-10827
    - add truth label for 1 vs 3 prong tau


??? example "Tau ID"

    goals: harmonise workflows with tau cp

    - add truth label for 1 vs 3 prong tau
    - test if we need R=0.2 jets for tau reconstruction, or whether we can use harmonised R=0.4


??? example "Work out how to include jet constituents"

    - adding netural flow elements (FE) improves performance
    - improved track parameters on charged FE also help
    - the track selection on the charged FE is tighter than FTAG to we need to supplement with the loose tracks
    - getting this running in athena could be a bit of a pain so we need to think about how best to handle this
    - test whether we can ignore the overlap and let the network figure it out
    - see [AFT-734](https://its.cern.ch/jira/browse/AFT-634)


??? exmaple "Gradient analysis for multitask learning"

    - at the moment loss weights for different tasks are fixed
    - we might be able to optimise weights on the fly based on the difficulty of each task
    - could speed up training and yield better performance by avoiding suboptimal tradeoffs
    - see the following:
        - [[2301.13501]](https://arxiv.org/abs/2301.13501) 
        - [[1711.02257]](https://arxiv.org/abs/1711.02257)
        - [[1912.06844]](https://arxiv.org/abs/1912.06844)
        - [[2203.06801]](https://arxiv.org/abs/2203.06801)


??? exmaple "Remove the preprocessing step"

    - at the moment resampling is done in a standalone preprocessing step
    - can we replace resampling with on-the-fly reweighting?



### Smaller Studies:


??? example "ROC curves with SF applied"

    - technically possible in puma now
    - aim for run-3 calib paper?

??? example "Network architecture optimisations"

    goals: Improve performance of the taggers

    - [compare normalisation styles](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt/-/issues/33)
    - compare activation functons (relu vs silu vs mish)
    - larger scale [hyperparameter optimisation](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt/-/issues/25)

??? example "Feature importance studies"

    goals: reduce the number of input features to the taggers without compromising performance
    
    - could rerun training with features removed
    - or use more advanced methods outlined [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt/-/issues/29)
    - new or unexploited input features could also be considered
