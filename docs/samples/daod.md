# Derivation

The FTAG group maintains several different derivation formats for algorithm training and various performance studies.
Different formats are specialized for different "performance study" objectives, your target objective dictates the format to be used/requested. More details in the tables below.

## Which derivation format should I use?

First of all, you might not want `FTAG` derivations at all:

- Most of the flavor tagging calibrations should be using `DAOD_PHYS`, unless otherwise stated below.
- No physics analysis should be using `FTAG` derivaitons.

Pay attention to the purpose: if your use case isn't explicitly listed below we make no promises to support it.

### List of FTAG derivations and use

Note that though we have the same derivation format name as in R21, the purpose and content for each is different.

Extra content is given with respect to `PHYS`

| Format | Extra                   | Skimming | Thinning | Purpose               | Other Notes |
| ------ | ------------------------------- | -------- | -------- | --------------------- | ----------- |
| FTAG1  | :curly_loop: :black_circle: P J | None     | None     | Algorithm Development | MC Only     |
| FTAG2  | :curly_loop: :black_circle: P J | $e +\mu$ | Only GA tracks | Algorithm cross checks in data | Can also support ttbar based b-jet cablibration |
| FTAG3  | :curly_loop: :black_circle: P J | $j \| \mu$ | None | $g \to bb$ Xbb calibation | |
| FTAG4  | None | single lepton | `PHYS` | FTAG $b$, $c$ and light-jet calibations | |

Extra content key

| Symbol | meaning |
| ------------ | ------ |
| :curly_loop: | tracks |
| :black_circle: | calorimeter clusters |
| P | pflow objects |
| J | All jet and b-jet information |

## An Essential Three Step Guide to Requesting Derivations

1. 	Given an MR, make sure it's merged and has been swept to main (often FTAG MRs target 24.0, but the derivations use main, so we need to wait for the sweep). 

2. If the current release don't have MRs of interest included, we need to request to build a new release. A template of how to do this is here: [ATLINFR-5229](https://its.cern.ch/jira/browse/ATLINFR-5229) (and most recently in [ATLINFR-5370](https://its.cern.ch/jira/browse/ATLINFR-5370)).

3. DAOD Production Requests: When the release is built, open a ticket following procedures laid out under DAOD Production Requests. 
- JIRA space Link to request derivations on MC: [ATLFTAGDPD](https://its.cern.ch/jira/projects/ATLFTAGDPD)  
- JIRA space Link to request derivations on data (or large collection of PHYS derivations on MC) : [ATLASDPD](https://its.cern.ch/jira/projects/ATLASDPD)


## DAOD Production Requests

??? abstract "Derivation Requests: Best Practices!"

    Please be advised that one should only request derivations from the FTAG side when there is a genuine FTAG interest. 
    Regular Physics Analysis derivation production is done by group contacts for that relevant group and shouldn't be requested from the FTAG side, unless, in the rare case (exceptionally), FTAG plays a major role for that specific exercise jointly with the Physics Analysis in question.
    This otherwise incurs a wrath from higher powers that can't be neglected.

If you need a new release after a critical MR to fix a bug and/or add/alter the derivation content, please ask the FTAG (subgroup) conveners to open a ticket in this JIRA space: [ATLINFR](https://its.cern.ch/jira/browse/ATLINFR)

### Derivation requests on MC samples (FTAGx/PHYS etc)

One should open a ticket under [ATLFTAGDPD](https://its.cern.ch/jira/projects/ATLFTAGDPD).

A template is available here: [ATLFTAGDPD-377](https://its.cern.ch/jira/browse/ATLFTAGDPD-377)

Please be sure to include the following information:

- Tag FTAG GROUP derivation contacts with JIRA tag handles `@sgoswami` & `@waislam`
- Which samples you are requesting and why (e.g. ttbar and Z' samples for training).
- Which derivation format (e.g. FTAG1).
- Which release you would like the production in (e.g. `25.0.x`) and the corresponding p-tag.
- A list of full 3-tag recon AOD names, split up by MC campaign
- Note that merged/repeated tag samples with double or triple e/s/r tags aren't used for Derivation production (like `*r13145_r13144` etc.)
- The list can be either in a collaborative CodiMD file or a plain text file uploaded to the JIRA or in the description itself.

Recon AOD names would appear like

```bash
mc23_13p6TeV.<6-digit-DSID>.<Physics Process Short Name>.recon.AOD.<e tag - 4 digits>.<s-tag - 4 digits>.<r-tag - 5 digits>
```

The [FTAG derivation contacts](https://ftag-sw.docs.cern.ch/DAOD/#ftag-derivation-contacts) will then respond to your request.

??? abstract "Template FTAG1 Request on MC"

    Dear `<tag ftag derivation contacts>`

    We would like to request FTAG1 derivations for `<samples>` in release `<24.0.x>` (`<p-tag>`).

    The list of samples is below.

    *MC23:*
    ```
    mc23_13p6TeV.....
    mc23_13p6TeV.....
    mc23_13p6TeV.....
    ```

    Cheers,
    <name>

    tagging <interested parties>

###  Derivation requests on data (FTAGx/PHYS etc)

One should open a ticket under [ATLASDPD](https://its.cern.ch/jira/projects/ATLASDPD).
Please be sure to include the following information:

- Tag all the Central derivation contacts ( JIRA tag handles of current CENTRAL derivation contacts are: `@emmat` `@fladias` & `@maklein` )
- Data containers you are requesting the derivations on and why.
- Which derivation format (e.g. FTAG1).
- Which release you would like the production in (e.g. `25.0.x`) and the corresponding p-tag.
- PHYS derivation requests on data containers need to be discussed with the Central Derivations Team before submission on their end.

The [CENTRAL derivation contacts](https://ftag-sw.docs.cern.ch/DAOD/#central-data-derivation-contacts) will then respond to your request.

??? abstract "Template PHYS Request on data"

    Dear `<tag central derivation contacts>`

    We would like to request PHYS derivations for `<data>` in release `<24.0.x>` (`<p-tag>`).

    The list is below.

    data15_13TeV.periodAllYear.physics_Main.PhysCont.AOD.repro26_v01

    Cheers,
    <name>

    tagging <interested parties>


#### Querying AOD datasets

One can use the following snippets to check full AOD names

#### Yelp! AOD samples don't exist or got deleted on the grid or are on TAPE!!!

You would need to get hold of FTAG MC contacts and request the production of AODs if they don't exist.
This becomes much simpler if `HITS` files aren't deleted (and infinitely complicates the production if the `HITS` files are deleted from the grid).

If they are on `TAPE`, you would need to ask FTAG MC contacts to submit an `r2d2` request via [RUCIO Web UI](https://rucio-ui.cern.ch/) to restore the samples to some grid `CLOUD` storage element for rapid access.


#### Querying all possible AOD dataset names mapped to a DSID

You can simply run (after changing mc20/mc23 and <6-digit-DSID> to suit your needs):

```bash
setupATLAS -q
voms-proxy-init --voms atlas
lsetup rucio
lsetup panda
lsetup pyami

rucio ls mc20_13TeV.<6-digit-DSID>.*.recon.AOD.* | grep "CONTAINER" | grep -v "log"
rucio ls mc23_13p6TeV.<6-digit-RUN3-DSID>.*.recon.AOD.* | grep "CONTAINER" | grep -v "log"

```

#### What if you have raw sample names without e/s/r-tags?

You can run the snippet below to get 3-tag `recon` AOD names, say, with `r tag` `r13145`
where `raw_samp_names.txt` has one raw sample name per line.

Snippets here may also be edited to make more general queries using `rucio`.

[ATLAS AMI](https://ami.in2p3.fr/) provides verbose info (production/campaign version) on your sample `e/s/r/p` tags.

```bash
setupATLAS -q
voms-proxy-init --voms atlas
lsetup rucio
for line in $(cat raw_samp_names.txt); do
        templist=$(rucio ls "$line"* | grep "recon" | grep -v "log" | grep "CONTAINER" | cut -c 2- | grep -E '.AOD\.e[0-9]{4}_[s][0-9]{4}_r[0-9]{5}.*' | rev | cut -c 22- | rev | grep -E "r13145" | sed 's/^[ \t]*//;s/[ \t]*$//')
        echo "$templist" >> fullsamplenames.txt
done
```
#### What if you want to check that a derivation sample exists?

Change mc20, <6-digit-DSID>, FTAG1, 5770 to suit your needs.
Look for samples with each of single e,s,r & p tags

```bash
setupATLAS -q
voms-proxy-init --voms atlas
lsetup rucio
rucio ls mc20_13TeV.<6-digit-DSID>.*.deriv.DAOD_FTAG1.* | grep "CONTAINER" | grep -v "log" | grep "p5770"
```

#### What if you want to check that a derivation container is not empty?

```bash
setupATLAS -q
voms-proxy-init --voms atlas
lsetup rucio
rucio list-files <full derivation dataset name queried above>
```
If the output is `0.0B` then, the container nominally exists but it isn't populated, in which case proceed with the derivation request by opening a new JIRA ticket.


## Run Derivations

Derivation in r22 (and above) are produced using the `Derivation_tf.py` command using the CA mode.

### Run a local derivation

To begin, setup the release/nightly you would like the derivation to be launched.
```bash
setupATLAS
# only select one of the following options
asetup Athena,main,latest # if you want to test in the latest main 
asetup Athena,main,ryear-month-date # if you want to test a specific nightie, for example r2023-03-20
asetup Athena,25.0.5
```

To run a derivation routine locally, you'll need a local `AOD` sample. The most commonly used `AOD` files are listed in the [algorithm dataset overview](https://ftag.docs.cern.ch/software/data/) page.

Next, run a derivation
```bash
    Derivation_tf.py --CA \
    --formats FTAG1 \
    --inputAODFile path/to/AOD.root \
    --maxEvents 100 \
    --outputDAODFile test.root \
```

If you want to use the config from an existing p-tag you can include `--AMIConfig pXXXX`.


??? warning "The main branch is a moving target and may be broken"
    Take a look [here](https://training-dataset-dumper.docs.cern.ch/tests/#making-daods-from-aods) for instructions on how to find a previously used release.

If you're interested in whether ART tests are successful with a given release, feel free to take a peek at [ART Tests FTAG](https://bigpanda.cern.ch/art/jobs/?branch=main/Athena/x86_64-el9-gcc13-opt&nlastnightlies=7&view=packages&package=DerivationFrameworkFlavourTagART) for `FTAG` derivations.

You can also make your own selections in [the ART mnenu selection page](https://bigpanda.cern.ch/art/) with a proper `athena` release and `gcc` version. 
A helpful pdf is also provided [here](https://twiki.cern.ch/twiki/pub/AtlasProtected/DerivationProductionTeam/art_athderivation.pdf)

??? info "Making test DAOD files for use with the TDD"
    There is also a guide to making test DAODs for use with the TDD [here](https://training-dataset-dumper.docs.cern.ch/tests/#making-inputs-for-tests).

??? info "Turning on a flag for special studies"
    Here is an example.
    ```
    --preExec 'flags.BTagging.Trackless=True'
    ```

## Modify Derivations
Currently, all FTAG derivation related changes should target [main branch](https://gitlab.cern.ch/atlas/athena/-/tree/main) in athena.

```bash
setupATLAS
asetup Athena,main,latest
lsetup git
git atlas init-workdir ssh://git@gitlab.cern.ch:7999/atlas/athena.git -b main 
cd athena
git checkout -b main-my-test-branch upstream/main --no-track # to checkout a new branch for later making MRs
git push --set-upstream origin main-my-test-branch
git atlas addpkg DerivationFrameworkFlavourTag # or other packages you want
## make changes ##
cd ../ && mkdir build && cd build
cmake ../athena/Projects/WorkDir/
make 
source x*/setup.sh
## compile to do a local test, to make sure the change works ##
cd ../ && mkdir run && cd run
## then here in the run directory to perform a local test
```

### Derivation with hit information

This section documents the instructions for creating FTAG1 derivation that stores hit information. 

It contains two steps, the first is to generate AOD sample from HITS file, the second is to generate the FTAG1 derivation from the AOD.

A detailed description of the task can be found in [AFT-579](https://its.cern.ch/jira/browse/AFT-579) and [AFT-572](https://its.cern.ch/jira/browse/AFT-572?focusedCommentId=4038020&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-4038020).

#### HITS &rarr; AOD 
First an AOD that includes hit information needs to be created.

- To create an AOD with 100 events from a HITS file the following command can be used. 

- The RDO files need to be downloaded locally with `rucio`.

- `Reco_tf.py` used below is the ATLAS general purpose reconstruction transform. It is able to process RAW and RDO through to AODs.

??? abstract "Example for HITS to AOD conversion"
    ```
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c x86_64-centos7-gcc11-opt
    asetup --platform=x86_64-centos7-gcc11-opt Athena,22.0.104
    Reco_tf.py \
    --inputHITSFile="HITS.24937557._019634.pool.root.1" \
    --asetup="RDOtoRDOTrigger:Athena,21.0.20.12" \
    --multithreaded="True" \
    --postInclude "default:PyJobTransforms/UseFrontier.py" "all:PyJobTransforms/DisableFileSizeLimit.py" \
    --preExec "all:flags.BTagging.Trackless = True; from InDetPrepRawDataToxAOD import InDetDxAODJobProperties; InDetDxAODJobProperties.DumpTruthInfo=True; " \
    --preInclude "all:Campaigns/MC20a.py" \
    --skipEvents="0" --autoConfiguration="everything" \
    --conditionsTag "default:OFLCOND-MC16-SDR-RUN2-09" "RDOtoRDOTrigger:OFLCOND-MC16-SDR-RUN2-08-02a" \
    --geometryVersion="default:ATLAS-R2-2016-01-00-01" \
    --runNumber="800030" \
    --digiSeedOffset1="18231" \
    --digiSeedOffset2="18231" \
    --AMITag="r14233" \
    --outputAODFile="AOD.24937557._019634.pool.root.1" \
    --jobNumber="18231" \
    --steering "doRAWtoALL" \
    --triggerConfig="RDOtoRDOTrigger=MCRECO:dbf:TRIGGERDBMC:2283,35,327" \
    --inputRDO_BKGFile="RDO.26835812._026569.pool.root.1,RDO.26835812._026570.pool.root.1,RDO.26835812._026571.pool.root.1,RDO.26835812._026572.pool.root.1,RDO.26835812._026573.pool.root.1"
    --maxEvents="100" 
    ```



Jet and BTagging containers are built at reconstruction level but not saved in an AOD by default. If you want to save such containers, the following `preExec` should be included (in CA mode):

```
--preExec "all:ConfigFlags.Jet.WriteToAOD=True;"
```

Please note that `--AMITag` argument takes care of certain redundantly supplied arguments such as `triggerConfig`, `geometryVersion`, `conditionsTag`, possibly others.
The extra arguments should only be supplied if you want to test something non-standard. Often, certain non-standard arguments are incompatible together.

One can also use, say, `--AMIConfig r14159` to bypass passing the extra arguments; the corresponding tag info can be looked up [here](https://atlas-ami.cern.ch/?subapp=tagsShow&userdata=r14159) on the (A)tlas (M)etadata (I)nterface. 
For a template, please have a look [here](https://gitlab.cern.ch/atlas/athena/-/blob/24.0/PhysicsAnalysis/JetTagging/FlavourTaggingTests/test/test_FTAG_AOD_withtrackless_MC_grid.sh)

The `flags.BTagging.Trackless = True` setting in the `--preExec` adds the hit information (Pixel+SCT) to the AOD. Additional settings (e.g. the pT threshold for the jet-hit association can be set following the definitions in [BTaggingConfigFlags.py](https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/JetTagging/JetTagConfig/python/BTaggingConfigFlags.py#L92)).  

The `InDetDxAODJobProperties.DumpTruthInfo=True` setting is necessary for saving the `truth_barcode` information for the hits.


#### AOD &rarr; FTAG1 derivation 
To create an FTAG1 derivation file from the file created above, the following commands can be used

```bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup Athena,25.0.6
export ATHENA_PROC_NUMBER=8
Derivation_tf.py --CA --inputAODFile input.AOD.pool.root --outputDAODFile output.pool.root --formats FTAG1
```
#### Running derivations on the grid for testing
Please do not use this method to produce derivations for full datasets, only use it for testing purposes.

```bash
setupATLAS -q
voms-proxy-init --voms atlas
lsetup rucio
lsetup panda
lsetup pyami
pathena --trf "Derivation_tf.py --CA --inputAODFile=%IN --outputDAODFile=%OUT.pool.root --maxEvents=5000 --skipEvents=0 --formats=PHYSLITE" \
 --inDS mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_s3681_r12960_r12963 \
 --outDS user.username.410470.PHYSLITE_CA_v2 
```

