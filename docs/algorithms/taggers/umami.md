# Umami

The Umami tagger is a continuation from the DL1d tagger, extending it by treating the DIPS and the DL1 block as a combined network which is trained as a whole.

The Umami tagger architecture is shown in the illustration below. It consists of a [DIPS](https://ftag.docs.cern.ch/algorithms/dips/) block whose output is processed together with jet features by a fully connected deep neural network inspired by the [DL1](https://ftag.docs.cern.ch/algorithms/dl1/) architecture.

![Umami architecture](../../assets/umami_architecture.png)

