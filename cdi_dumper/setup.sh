# setup ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

echo "=== running setupATLAS ==="
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

echo "=== running asetup ==="
CI=.gitlab-ci.yml
DOCKER_ANALYSIS_BASE_VERSION=$(
    sed -rn '/analysisbase/ s/.*:([^-]*).*/\1/ p' $CI | head -n1)
unset CI

if [[ ! $DOCKER_ANALYSIS_BASE_VERSION =~ [0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    echo "couldn't parse CI file for base image, setting up latest"
    unset DOCKER_ANALYSIS_BASE_VERSION
fi

asetup AnalysisBase,${DOCKER_ANALYSIS_BASE_VERSION-"master,latest"}
