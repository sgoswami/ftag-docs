# High-level tagger operating point definition

The flavour-tagging algorithms provide a discriminant which is used to select heavy-flavour jets and discriminate them against light-flavour and c-jets (or b-jets in case of charm-tagging).
The cut value of the discriminant corresponds to certain operating points (OPs) of the algorithm, which are recommended by the FTAG group. Informally, these cut values are also referred to as working points (WPs).

The definition and studies about providing an operating point with a non-flat b-tagging efficiency are documented here.


