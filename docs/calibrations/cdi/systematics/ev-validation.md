# Validating the eigenvector decomposition methods

The eigenvector variations produced from either the `SFEigen` or `SFGlobalEigen` systematic strategies should account for the majority of the variation (or *variability*) of the original set of systematic uncertainties derived from the calibration analyses. An eigenvector variation $v_{i}$, produced from the PCA, can be said to "capture" a certain percentage of the total variation by taking the ratio of its associated eigenvalue $\lambda_{i}$ with the sum of all eigenvalues, that is:

$$
C_{i} = \frac{\lambda_{i}}{\sum_{j} \lambda_{j}}
$$

**Note: The eigenvalue of an eigenvector variation is equivalent to its variance as the eigenvalues form the diagonal entries of the covariance matrix in the new eigenvector basis - this property stems from the orthogonality of the eigenvectors produced in the PCA.** 

The eigenvalues of the eigenvector variations also determine the order by which the eigenvector variations are stored within `CalibrationDataEigenVariations` : they are sorted by decreasing $|\lambda|$. Normally (in `SFEigen`), the first 10 eigenvectors combined account for nearly $100\%$ of the total variation. However, with each additional eigenvector variation, the "model" of the bin-to-bin correlations can greatly improve while the additional captured variability improves in small increments.


One way to cross-check the ability of a set of eigenvector variations is to *reconstruct* the total covariance matrix from this reduced set of variations. This *approximate* covariance matrix can then be compared directly with the total covariance matrix of the full set of systematic uncertainties - and in this way, **one can validate the effectiveness of the eigenvector variation reduction schemes.**


??? note "The relative percentage error in the `SFEigen` strategy for a b-jet."
    Below you can see a one-to-one comparison of the total covariance matrix to the approximate covariance matrix that can be constructed from the reduced set of (9) eigenvector variations. 
    
    Letting $C$ be the "true" total covariance matrix, and $C'$ be the "approximate" total covariance matrix, then the plot below shows the relative percentage error as computed by 

    $$
    Err(C, C') = \frac{C - C'}{C} \times 100\%
    $$
    
    
    The plot below essentially shows how effective the bin-to-bin correlations are preserved when performing the `SFEigen` PCA on a fixed cut calibration working point. **Note: The color bar in these plots are on a $log_{10}$ scale, thus the largest percentage error in the plot below is around $10^{-4}\%$.**

    ![Relative Error](../../../assets/cdi/BjetSFEigenRelativeError.png)
    
   
    Here you can see the same comparison for the b-jet continuous calibration for the 'SFEigen' strategy. **Note: The number of variations is substantially increased (45), and while we still capture nearly $100\%$ of the total variability, the largest deviation in the model of the bin-to-bin correlations produced by this reduced set of eigenvector variations is *larger* than in the fixed-cut working point, at most around $10^{-2}\%$.**

    ![Continuous Relative Error](../../../assets/cdi/BjetSFEigenRelativeError2.png)


