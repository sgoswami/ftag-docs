from os.path import join
from shutil import copyfile
from subprocess import run

if __name__ == "__main__":
    # copy CDI file summary for deployment
    run(["mv", "cdi_dumper/output/plots", "docs/recommendations/calib/plots"])
    sample_source = join("cdi_dumper/output", "md", "cdi_overview.md")
    sample_target = join("docs", "recommendations", "calib", "cdi_overview.md")
    copyfile(sample_source, sample_target)

    sample_source = join("cdi_dumper/output", "md", "cdi_plot_overview.md")
    sample_target = join("docs", "recommendations", "calib", "cdi_plot_overview.md")
    copyfile(sample_source, sample_target)
