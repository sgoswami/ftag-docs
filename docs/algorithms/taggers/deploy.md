# Deploying Models in Athena

After you have trained your tagger, you will want to deploy it in Athena. This page explains how to do that.
There are a few steps to follow:

1. Serialise your model to a deployable format
2. Add the model to the group area
3. Schedule the model to run during derivations

!!! warning "Please follow the steps listed here carefully"
    
    The deployment of models in Athena is a critical step in the process of using them in the ATLAS software framework.
    It is important to follow the steps listed here carefully to ensure that the models are deployed correctly and can be used by the rest of the collaboration.

??? info "How does my model actually run in Athena?"

    https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/GNN.h

    The networks are scheduled using [`DL2`](https://gitlab.cern.ch/atlas/athena/-/tree/master/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants#inputs-and-outputs-from-dl2).

    ??? info "Resolving paths using `PathResolver`"

        Using the `PathResolver`, the path of the network is searched for (in the following order) 

        - in the local directory where you start the job
        - `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/` (mirror of `/eos/atlas/atlascerngroupdisk/asg-calib/`)
        - on https://atlas-groupdata.web.cern.ch/atlas-groupdata/ (works in docker images)

## 1. Model Serialisation

Older models (including DIPS and DL1x) are serialised to [`lwtnn`](https://github.com/lwtnn/lwtnn) format.
These models are generally trained with Umami, and you can find more info
about the export process [here](https://umami-docs.web.cern.ch/trainings/LWTNN-conversion/).

Newer models trained with Salt (including GN2) are exported to [`ONNX`](https://onnx.ai).
A guide on exporting trained models is available [here](https://ftag-salt.docs.cern.ch/export/),
please follow it closely.


## 2. Add Models to Group Area


The trained models are hosted in the ATLAS FTAG group area.

The `dev/BTagging/` space is used for testing within the FTAG group, while the main `BTagging/` space is used for deployment to the whole collaboration.

??? info "Paths to the Group Space"

    === "EOS"
        Base path on EOS:
        ```
        /eos/atlas/atlascerngroupdisk/asg-calib/
        ```

        === "FTAG Group Space EOS"
            Group space EOS:
            ```
            /eos/atlas/atlascerngroupdisk/asg-calib/BTagging/
            ```
            Web interface: [https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/)

        === "FTAG Dev Space EOS"
            Dev space EOS:
            ```
            /eos/atlas/atlascerngroupdisk/asg-calib/dev/BTagging/
            ```
            Web interface: [https://atlas-groupdata.web.cern.ch/atlas-groupdata/dev/BTagging/](https://atlas-groupdata.web.cern.ch/atlas-groupdata/dev/BTagging/)

    === "CVMFS"
        Base path on CVMFS:
        ```
        /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/
        ```

        === "FTAG Group Space CVMFS"
            Group space CVMFS:
            ```
            /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/BTagging/
            ```

        === "FTAG Dev Space CVMFS"
            Dev space CVMFS:
            ```
            /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/BTagging/
            ```

### What to copy and where

To deploy your model to the group space, you need to first make the relevant files available on EOS so that they can be copied to the group space by the flavour tagging [algorithm sub-group conveners](mailto:atlas-cp-flavtag-algorithms-conveners@cern.ch).
You need to make the following files available:

=== "Salt / ONNX"

    Since Salt saves metadata directly in the ONNX model, you only need to make the ONNX model available on EOS.
    
    - The exported `network.onnx` model, which contains all necessary metadata. This will be copied to the FTAG group area
    - Your pytorch checkpoint file. This is coped to the `umami` account space on EOS.
    - Your YAML training config. This is coped to the `umami` account space on EOS.

=== "Umami / lwtnn"

    Since the lwtnn model does not contain metadata, you should make the following items available on EOS:

    - keras/pytorch model files obtained from training the algorithm (but of course not the training and evaluation samples)
      - for the pytorch model, please copy both the DGL and the Vanilla pytorch model
    - converted model (`lwtnn` or `onnx`)
    - scale dictionary from umami (`scale_dict.json`)
    - variable config from umami (`variables.yaml`)


Please also specify whether the model is ready for deployment in `BTagging/` for the whole collaboration or if it should be deployed in the `dev/BTagging` (development) area for testing within the FTAG group.


### Destination paths

**Please ensure** that the destination path in the group area is formatted as below.

- `BTagging/<timestamp>/<algorithm name>/<jetcollection>/network.onnx`

!!! danger "Warning: if the model path is not formatted correctly, things will break!"


!!! info "Conveners: info on copying group to area can be found [here](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ASGCalibArea)"

    Remember that only the serialised model should be copied to the group area.
    Other files, such as training configs, should be copied to the `umami` account space on EOS.

The convener will then copy **only** the `network.onnx` model file to the **correct path** in the group area.

!!! info "The synchronisation of the group area on `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/` with the files on `/eos/atlas/atlascerngroupdisk/asg-calib/` can take a few hours."

Finally, the convener will copy the pytorch checkpoint and training configs to the `umami` account space on EOS.

- `/eos/home-u/umami/models/`


## 3. Schedule the Model to Run During Derivations

You need to do two things to run the tagger in derivations:

1. Schedule the tagger to run for the relevant jet collection in [`BTagConfig.py`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagAlgs/BTagging/python/BTagConfig.py)
2. Add the tagger outputs to the listed of saved variables in [`BTaggingContent.py`](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/DerivationFramework/DerivationFrameworkFlavourTag/python/BTaggingContent.py)

Some noteable examples of models being added to derivations are:

- GN1 atlas/athena!54403
- GN2v00 atlas/athena!61571
- GN2v01 atlas/athena!67592

??? info "Multifold models are handled differently"

    Take a look at GN2v01 as an example GN2v01: atlas/athena!67592.

